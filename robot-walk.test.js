const robotWalk = require("./robot-walk").robotWalk;
const assert = require('assert');

describe("Given that I have provided robotWalk function parameters ", () => {
  const testCase1 = [ 1, 2, 4 ];
  const expectedCase1 = { x:2,  y: -3 };

  it("When the parameters [1, 2, 4] are result should be [2, -3]", async () => {

    const actualCoordinates1 = await robotWalk(testCase1);
    console.log('actualCoordinates1 ',actualCoordinates1)
    assert(actualCoordinates1[0] === expectedCase1.x);
    assert(actualCoordinates1[1] === expectedCase1.y);
  });

  const testCase2 = [ 1, 2, 4, 1, 5 ]
  const expectedCase2 = { x: 1, y: 1 };

  it("When the parameters [1, 2, 4, 1, 5] are result should be [1, 1]", async () => {
    const actualCoordinates2 = await robotWalk(testCase2);
    console.log('actualCoordinates2 ',actualCoordinates2)
    assert(actualCoordinates2[0] === expectedCase2.x);
    assert(actualCoordinates2[1] === expectedCase2.y);
  });
});
