module.exports.robotWalk = (instructions) => {
  console.log('Init robotWalk function with instructions[ '+ instructions.toString() + ' ].');
  const coord = { x: 0, y: 0 } ;
  let orientationCache = { o: 'N' };
  let pathCache = new Set();
  pathCache.add(Object.values(coord).toString());

  return new Promise((resolve, reject) => {
    for(let instruction of instructions) {
      switch(orientationCache.o){
        case 'N':
          while(instruction--) {
            coord.y += 1;
            if(pathCache.has(Object.values(coord).toString())) {
              return resolve(Object.values(coord));
            }
            pathCache.add(Object.values(coord).toString());
          };
          orientationCache.o = 'E';
          break;
        case 'E':
          while(instruction--) {
            coord.x += 1;
            if(pathCache.has(Object.values(coord).toString())) {
              return resolve(Object.values(coord));
            }
            pathCache.add(Object.values(coord).toString());
          };
          orientationCache.o = 'S';
          break;
        case 'S':
          while(instruction--) {
            coord.y -= 1;
            if(pathCache.has(Object.values(coord).toString())) {
              return resolve(Object.values(coord));
            }
            pathCache.add(Object.values(coord).toString());
          }
          orientationCache.o = 'W';
          break;
        case 'W':
          while(instruction--) {
            coord.x -= 1;
            if(pathCache.has(Object.values(coord).toString())) {
              console.log('has coords');

              return resolve(Object.values(coord));
            }
            pathCache.add(Object.values(coord).toString());
          };
          orientationCache.o = 'N';
        break;
      }
    }
    return resolve(Object.values(coord));
  })
}
