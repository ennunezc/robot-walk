# Robot Walk

## Dependencies
* `mocha`

## Set up
`git clone git@gitlab.com:ennunezc/robot-walk.git`

`cd robot-walk`

`npm i`

## Test

`npm run test`

Done :)